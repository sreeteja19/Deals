package com.deals.core;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class WriteToXml {

	public void toXML(Object jaxb) throws JAXBException {
		String fileName = "GetInfoByZipResponse.xml";
		JAXBContext context = JAXBContext.newInstance(jaxb.getClass());
		Marshaller marshall = context.createMarshaller();
		marshall.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshall.marshal(jaxb, new File(fileName));
	}

}
