package com.deals.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQItem;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;

import org.w3c.dom.Node;

import com.saxonica.xqj.SaxonXQDataSource; 


public class QueryXml {

	public  String extractCityOrState(String cityOrState) throws FileNotFoundException, XQException {
		
		String fileName = null;
		if(cityOrState.equalsIgnoreCase("city")) {
			fileName = "extractCity.xqy";
		}
		else if(cityOrState.equalsIgnoreCase("state")) {
			fileName = "extractState.xqy";
		}
		
		InputStream inputStream = new FileInputStream(new File(fileName));
		XQDataSource ds = new SaxonXQDataSource();
		XQConnection conn = ds.getConnection();
		XQPreparedExpression exp = conn.prepareExpression(inputStream);
		XQResultSequence result = exp.executeQuery();
		
		String response = null;
		
		if (result.next()) {
			XQItem item = result.getItem();
			Node node = item.getNode();
			response = node.getTextContent();
		}
		return response;
	}
}
