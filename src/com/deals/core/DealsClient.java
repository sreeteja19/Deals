package com.deals.core;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;
import javax.xml.xquery.XQException;

import net.webservicex.GetInfoByZIPResponse;
import net.webservicex.GetInfoByZIPResponse.GetInfoByZIPResult;
import net.webservicex.USZip;
import net.webservicex.USZipSoap;

public class DealsClient {

	public static void main(String[] args) {
		String zip = "30328";
		getInfoByZipCode(zip);
		String city = getCityOrState("City");
		String state = getCityOrState("State");
		System.out.println("State: " + state);
		System.out.println("City: " + city);

	}

	public static void getInfoByZipCode(String zipCode) {
		
		WriteToXml writeToXml = new WriteToXml();
		USZipSoap zipResult = new USZip().getUSZipSoap();
		GetInfoByZIPResult result = zipResult.getInfoByZIP(zipCode);
		GetInfoByZIPResponse response = new GetInfoByZIPResponse();
		response.setGetInfoByZIPResult(result);
		
		try {
			writeToXml.toXML(response);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	public static String getCityOrState(String cityOrState) {
		
		QueryXml query = new QueryXml();
		String result = null;
		
		try {
			result = query.extractCityOrState(cityOrState);
		} catch (FileNotFoundException | XQException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
