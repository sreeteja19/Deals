package com.deals.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DealsClientTest {
	
	@Before
	public void setUp() {
		DealsClient.getInfoByZipCode("30328");
	}
	
	@Test
	public void testCity() {
		String expected = "Atlanta";
		String actual = DealsClient.getCityOrState("City");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testState() {
		String expected = "GA";
		String actual = DealsClient.getCityOrState("State");
		assertEquals(expected, actual);
	}

}
